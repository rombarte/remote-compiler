// Use WebSocket to connection with server

var connection;

function checkSettings() {
    // First, check if users browser can't connect to server
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser.");
    }
    else {
        $("#information").html("Sorry! Your browser don't support WebSockets. Upgrade it to the latest version.");
        $("input").attr("disabled", "");
    }
}

function onConnection() {
    var address = $("[name=url").val();
    var port = $("[name=port").val();
    var password = $("[name=password").val();

    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");

        try {
            connection = new WebSocket("ws://" + address + ":" + port);
        }

        catch (e) {
            console.log(e.message);
        }

        connection.onopen = function()
        {
            connection.send(password);
            console.log("Password was sent to server.");
        };

        connection.onmessage = function (evt) 
        { 
            var message = evt.data;
            console.log("Message was received from server.");

            if (message == "PASS_WRONG") {
                // Client is not authorized
                onPasswordWrong();  
            }

            else if (message == "PASS_OK") {
                // Client is authorized
                onPasswordOk();
            }

            else if (message == "COMM_OK") {
                // Client was send a right command
				connection.send(editor.getValue()); 
            }

            else if (message == "COMM_WRONG") {
                // Client was send a wrong command
                alert("Wrong command!");
            }

            else {
                // Client receive compilation result
                $('#output').val(message);  
            }
        };

        connection.onclose = function()
        { 
            console.log("Connection was closed.");
            onPasswordWrong();
        };

        connection.onerror = function (evt) 
        {
            console.log("Connection error!");
            alert("Connection error!");
        };
    }
	
    else
    {
        alert("Sorry! Your browser don't support WebSockets. Upgrade it to the latest version.");
    }
    
}

function onPasswordWrong() {
    $("#authentication").show();
    $("#development").hide();
    $("#information").html("You must use valid password!");
}

function onPasswordOk() {
    $("#authentication").hide();
    $("#development").show();
}

function Compile() {
    if ($("#compilerType").val() == "gcc") {
        connection.send("GCC_COM");
    }

    else if ($("#compilerType").val() == "g++") {
        connection.send("G++_COM");
    } 
}

function Run() {
    if ($("#compilerType").val() == "gcc") {
        connection.send("GCC_RUN");
    }

    else if ($("#compilerType").val() == "g++") {
        connection.send("G++_RUN");
    }
}

// Try to connect => login
$("input[name=connect]").click(function () {
    if ($("input[name=url]").val() && $("input[name=port]").val()) {
        onConnection();
    }
    else {
        $("#information").html("Server address and port can not be empty!");
    }
});

$("input[name=close]").click(function () {
    connection.close();
    onPasswordWrong();
});


$("input[name=url], input[name=port], input[name=password]").keydown(function (e) {
    var code = e.keyCode || e.which;
    if (code == '13') {
        if ($("input[name=url]").val() && $("input[name=port]").val()) {
            onConnection();
        }
        else {
            $("#information").html("Server address and port can not be empty!");
        }
    }
});

checkSettings();

var editor = ace.edit("code");
editor.setTheme("ace/theme/eclipse");
editor.getSession().setMode("ace/mode/c_cpp");
editor.setValue("int main(){\n\t //Write your code here!\n}");
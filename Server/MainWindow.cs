﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace PROJEKT_APLIKACJE_SIECIOWE_SERVER
{
    public partial class MainWindow : Form
    {
        private IPAddress serverIp;
        private int serverPort, serverConections;
        private Socket serverSocket, clientSocket;
        private Byte[] message, response;
        private string command;
        private StreamWriter logFile;
        private bool[] compilersState;
        private int runTimeout;

        /// <summary>
        /// Write log message to textbox and file.
        /// </summary>
        /// <param name="msg">Message text</param>
        private void addLogMessage(string msg)
        {
            textLog.Text += DateTime.UtcNow + ": " + msg + "\n";

            try
            {
                logFile.WriteLine(DateTime.UtcNow + ": " + msg + "\n");
            }

            catch
            {
                // Can not write to log file! But still writing to display...
            }
        }

        /// <summary>
        /// This method check, if compilators not exists. Do not show information for user.
        /// </summary>
        private void checkCompilersPath(TextBox[] path)
        {
            int iterator = 0;
            foreach (var compiler in path)
            {
                if (compiler.Text.Length != 0)
                {
                    compilersState[iterator] = true;
                    var proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = compiler.Text,
                            Arguments = "--version",
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            RedirectStandardError = true,
                            CreateNoWindow = true
                        }
                    };

                    try
                    {
                        proc.Start();
                        proc.Close();
                    }

                    catch
                    {
                        compilersState[iterator] = false;
                    }
                }

                else
                {
                    compilersState[iterator] = false;
                }

                iterator++;
            }
        }

        /// <summary>
        /// This method check, if compilators not exists. Show information for user.
        /// </summary>
        private void checkCompilersPath(string name, TextBox path, int arrayPosition)
        {
            if (path.Text.Length != 0)
            {
                compilersState[arrayPosition] = true;
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = path.Text,
                        Arguments = "--version",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };

                try
                {
                    proc.Start();
                    proc.Close();
                    MessageBox.Show(name + " compiler path is valid.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                catch
                {
                    addLogMessage(name + " compiler path is not valid!");
                    if (MessageBox.Show(name + " compiler path is not valid! Do you want download it?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        System.Diagnostics.Process.Start("http://tdm-gcc.tdragon.net/");
                    }
                    compilersState[arrayPosition] = false;
                }
            }

            else
            {
                addLogMessage(name + " compiler path is not valid!");
                if (MessageBox.Show(name + " compiler path is not valid! Do you want download it?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    System.Diagnostics.Process.Start("http://tdm-gcc.tdragon.net/");
                }
                compilersState[arrayPosition] = false;
            }
        }

        /// <summary>
        /// This method decode message from WebSocket protoccol.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string decodeMessage(Byte[] code)
        {
            Byte[] rawMessageData = null;
            Byte[] maskData = new Byte[4];
            ulong messageLength = code[1];

            if (code[0] == 129)
            {
                if (((messageLength - 128) <= 125) && ((messageLength - 128 >= 0)))
                {
                    // Message is small
                    messageLength -= 128;
                    rawMessageData = new Byte[messageLength];

                    for (ulong i = 0; i < messageLength; i++)
                    {
                        rawMessageData[i] = code[i + 6];
                    }

                    maskData[0] = code[2];
                    maskData[1] = code[3];
                    maskData[2] = code[4];
                    maskData[3] = code[5];
                }

                else if ((messageLength - 128) == 126)
                {
                    // Message is bigger
                    messageLength = (ushort)(code[2] << 8 | code[3]);
                    rawMessageData = new Byte[messageLength];

                    for (ulong i = 0; i < messageLength; i++)
                    {
                        rawMessageData[i] = code[i + 8];
                    }

                    maskData[0] = code[4];
                    maskData[1] = code[5];
                    maskData[2] = code[6];
                    maskData[3] = code[7];
                }

                else if ((messageLength - 128) == 127)
                {
                    // Message is the biggest
                    messageLength = (ulong)(code[2] << 56 | code[3] << 48 | code[4] << 40 | code[5] << 32 | code[6] << 24 | code[7] << 16 | code[8] << 8 | code[9]);
                    rawMessageData = new Byte[messageLength];

                    for (ulong i = 0; i < messageLength; i++)
                    {
                        rawMessageData[i] = code[i + 14];
                    }

                    maskData[0] = code[10];
                    maskData[1] = code[11];
                    maskData[2] = code[12];
                    maskData[3] = code[13];
                }

                else
                {
                    addLogMessage("Received data is broken!");
                    return "";
                }
            }

            else
            {
                addLogMessage("Received data is broken!");
                return "";
            }

            // Translating message bytes
            Byte[] decodedMessageData = new Byte[messageLength];
            for (ulong i = 0; i < messageLength; i++)
            {
                decodedMessageData[i] = (Byte)(rawMessageData[i] ^ maskData[i % 4]);
            }

            return Encoding.UTF8.GetString(decodedMessageData);
        }

        /// <summary>
        /// This method encode message to WebSocket protoccol.
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public Byte[] encodeMessage(Byte[] words)
        {
            Byte[] responseData = null;

            // Prepare header
            Byte[] lengthBytes = null;
            if ((words.Length <= 125) && (words.Length >= 0))
            {
                // Message is small
                responseData = new Byte[words.Length + 2];
                for (int i = 0; i < words.Length; i++)
                {
                    responseData[i + 2] = words[i];
                }

                responseData[0] = 129;
                responseData[1] = (byte)(words.Length);
            }

            else if ((words.Length > 125) && (words.Length <= 65535))
            {
                // Message is bigger
                lengthBytes = new byte[2];
                lengthBytes[0] = (Byte)((words.Length & 65280) >> 8);
                lengthBytes[1] = (Byte)((words.Length & 255));

                responseData = new Byte[words.Length + 4];
                for (int i = 0; i < words.Length; i++)
                {
                    responseData[i + 4] = words[i];
                }

                responseData[0] = 129;
                responseData[1] = 126;
                responseData[2] = lengthBytes[0];
                responseData[3] = lengthBytes[1];
            }

            else if (words.Length > 65535)
            {
                // Message is the biggest
                lengthBytes = new byte[8];
                lengthBytes[0] = (Byte)((words.Length & -72057594037927936) >> 56);
                lengthBytes[1] = (Byte)((words.Length & 71776119061217280) >> 48);
                lengthBytes[2] = (Byte)((words.Length & 280375465082880) >> 40);
                lengthBytes[3] = (Byte)((words.Length & 1095216660480) >> 32);
                lengthBytes[4] = (Byte)((words.Length & 4278190080) >> 24);
                lengthBytes[5] = (Byte)((words.Length & 16711680) >> 16);
                lengthBytes[6] = (Byte)((words.Length & 65280) >> 8);
                lengthBytes[7] = (Byte)((words.Length & 255));

                responseData = new Byte[words.Length + 10];
                for (int i = 0; i < words.Length; i++)
                {
                    responseData[i + 10] = words[i];
                }

                responseData[0] = 129;
                responseData[1] = 127;
                responseData[2] = lengthBytes[0];
                responseData[3] = lengthBytes[1];
                responseData[4] = lengthBytes[2];
                responseData[5] = lengthBytes[3];
                responseData[6] = lengthBytes[4];
                responseData[7] = lengthBytes[5];
                responseData[8] = lengthBytes[6];
                responseData[9] = lengthBytes[7];
            }

            else
            {
                addLogMessage("Input data is too big!");
                return null;
            }

            return responseData;
        }

        /// <summary>
        /// This method checks app configuration and run server.
        /// </summary>
        private void serverStart()
        {
            addLogMessage("Server is started.");
            // Check compilers exisiting
            checkCompilersPath(new TextBox[] { textPathGcc, textPathGpp });
            foreach (var compiler in compilersState)
            {
                if (compiler == false)
                {
                    if (MessageBox.Show("We have problem with one of compilers. Do you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        serverStop();
                        return;
                    }
                    break;
                }
            }

            try
            {
                // Server configuration
                serverIp = System.Net.IPAddress.Parse(textServerIp.Text);
                serverPort = int.Parse(textServerPort.Text);
                serverConections = 1;

                // Create serverSocket instance
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.SetIPProtectionLevel(IPProtectionLevel.Unrestricted);
                serverSocket.Bind(new IPEndPoint(serverIp, serverPort)); //153.19.209.35
                serverSocket.Listen(serverConections);
            }

            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            buttonStart.Text = "Stop server";
        }

        /// <summary>
        /// This metod accept clientSocket and make handshaking.
        /// </summary>
        private void acceptConnection()
        {
            // Connect to clientSocket with Handshaking
            clientSocket = serverSocket.Accept();
            addLogMessage("Client is connected.");
            clientSocket.ReceiveBufferSize = 2048000;

            message = new byte[clientSocket.ReceiveBufferSize];
            clientSocket.Receive(message);

            response = Encoding.UTF8.GetBytes("HTTP/1.1 101 Switching Protocols" + Environment.NewLine
                + "Connection: Upgrade" + Environment.NewLine
                + "Upgrade: websocket" + Environment.NewLine
                + "Sec-WebSocket-Accept: " + Convert.ToBase64String(
                    System.Security.Cryptography.SHA1.Create().ComputeHash(
                        Encoding.UTF8.GetBytes(
                            new System.Text.RegularExpressions.Regex("Sec-WebSocket-Key: (.*)").Match(ASCIIEncoding.ASCII.GetString(message)).Groups[1].Value.Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
                        )
                    )
                ) + Environment.NewLine
                + Environment.NewLine);
            // From: developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_server

            clientSocket.Send(response);

            bool authorized = checkAuthorization();
            if (authorized == true)
            {
                // Encode and send response
                Array.Clear(response, 0, response.Length);
                response = Encoding.UTF8.GetBytes("PASS_OK");
                clientSocket.Send(encodeMessage(response));
                addLogMessage("Client send right password.");

                onConnection();
            }
            else
            {
                // Encode and send response
                Array.Clear(response, 0, response.Length);
                response = Encoding.UTF8.GetBytes("PASS_WRONG");
                clientSocket.Send(encodeMessage(response));
                addLogMessage("Client send wrong password.");

                onAuthFailed();
            }

        }

        /// <summary>
        /// This method check user authorization.
        /// </summary>
        /// <returns></returns>
        private bool checkAuthorization()
        {
            // Get message from Client
            Array.Clear(message, 0, message.Length);
            clientSocket.Receive(message);
            decodeMessage(message);
            if (decodeMessage(message) == textPassword.Text)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method receive code and send compiler output until clientSocket is connected.
        /// </summary>
        private void onConnection()
        {
            button3.Enabled = true;
            while (clientSocket.Connected && !backgroundWorker.CancellationPending)
            {
                // Get message from Client
                Array.Clear(message, 0, message.Length);
                clientSocket.Receive(message);
                command = decodeMessage(message);
                addLogMessage("Received command: " + command);

                if (command == "GCC_COM" || command == "G++_COM" || command == "GCC_RUN" || command == "G++_RUN")
                {
                    Array.Clear(response, 0, response.Length);
                    response = Encoding.UTF8.GetBytes("COMM_OK");
                    clientSocket.Send(encodeMessage(response));

                    // Get message from Client
                    Array.Clear(message, 0, message.Length);
                    clientSocket.Receive(message);

                    addLogMessage("Received code from client.");

                    try
                    {
                        StreamWriter tempCodeFile = new StreamWriter("temp.cpp");
                        tempCodeFile.Write(decodeMessage(message));
                        tempCodeFile.Close();
                    }

                    catch
                    {
                        addLogMessage("Can not write temp code file!");
                    }

                    Process proc = null;
                    if (command == "GCC_COM" || command == "GCC_RUN")
                    {
                        proc = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = textPathGcc.Text,
                                Arguments = Directory.GetCurrentDirectory() + "\\temp.cpp -o temp.exe",
                                UseShellExecute = false,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                CreateNoWindow = true
                            }
                        };
                    }

                    else if (command == "G++_COM" || command == "G++_RUN")
                    {
                        proc = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = textPathGpp.Text,
                                Arguments = Directory.GetCurrentDirectory() + "\\temp.cpp -o temp.exe",
                                UseShellExecute = false,
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                CreateNoWindow = true
                            }
                        };
                    }

                    string output = "";
                    try
                    {
                        proc.Start();

                        while (!proc.StandardOutput.EndOfStream)
                        {
                            output += proc.StandardOutput.ReadToEnd();
                        }

                        while (!proc.StandardError.EndOfStream)
                        {
                            output += proc.StandardError.ReadToEnd();
                        }

                        proc.Close();

                        if (output.Length == 0)
                        {
                            
                            if (command == "G++_RUN" || command == "GCC_RUN")
                            {
                                proc = new Process
                                {
                                    StartInfo = new ProcessStartInfo
                                    {
                                        FileName = Directory.GetCurrentDirectory() + "\\temp.exe",
                                        UseShellExecute = false,
                                        RedirectStandardOutput = true,
                                        RedirectStandardError = true,
                                        CreateNoWindow = true
                                    }
                                };

                                proc.Start();
                                
                                if(proc.WaitForExit(runTimeout))
                                {
                                    while (!proc.StandardOutput.EndOfStream)
                                    {
                                        output += proc.StandardOutput.ReadToEnd();
                                    }

                                    while (!proc.StandardError.EndOfStream)
                                    {
                                        output += proc.StandardError.ReadToEnd();
                                    }    
                                }

                                else
                                {
                                    output = "Timeout! After " + runTimeout + " milliseconds app do not response.";
                                    try
                                    {
                                        proc.Kill();
                                    }

                                    catch
                                    {
                                        // Exception
                                    }
                                }

                                proc.Close();
                            }

                            else output = "0 errors.";
                        }

                        else
                        {
                            output = output.Replace(Directory.GetCurrentDirectory() + "\\temp.cpp:", "");
                        }

                        addLogMessage("Code compilation successful.");
                    }

                    catch
                    {
                        addLogMessage("Can not compile received code!");
                    }

                    // Encode and send response
                    Array.Clear(response, 0, response.Length);
                    response = Encoding.UTF8.GetBytes(output);

                    clientSocket.Send(encodeMessage(response));
                    addLogMessage("Code was sended to client.");
                }

                else
                {
                    Array.Clear(response, 0, response.Length);
                    response = Encoding.UTF8.GetBytes("COMM_WRONG");
                    try
                    {
                        clientSocket.Send(encodeMessage(response));
                        addLogMessage("Client send wrong command!");
                        break;
                    }
                    catch
                    {
                        addLogMessage("Client destroy a connection!");
                        break;
                    }

                }
            }

            clientSocket.Close();
            button3.Enabled = false;
            addLogMessage("Client was disconnected.");
            acceptConnection();
        }

        /// <summary>
        /// This method resolve problem with notauthorized user.
        /// </summary>
        private void onAuthFailed()
        {
            //clientSocket.Disconnect(true);
            clientSocket.Close();
            addLogMessage("Client was disconnected.");
            acceptConnection();
        }

        /// <summary>
        /// This method stops serverSocket and close all clients.
        /// </summary>
        private void serverStop()
        {
            if (clientSocket != null)
            {
                clientSocket.Close();
                addLogMessage("Client was disconnected.");
            }

            try
            {
                serverSocket.Close();
            }

            catch
            {
                // Nothing to do here...
            }

            addLogMessage("Server was stopped.");
            buttonStart.Text = "Start server";
        }

        public MainWindow()
        {
            InitializeComponent();
            compilersState = new bool[2];
            runTimeout = 1000;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (serverSocket == null)
                {
                    backgroundWorker.RunWorkerAsync();
                }

                else
                {
                    backgroundWorker.CancelAsync();
                    serverStop();
                    serverSocket = null;
                }

                button3.Enabled = false;
            }

            catch
            {
                addLogMessage("[Warning] System is busy.");
            }
        }

        private void buttonGetIp_Click(object sender, EventArgs e)
        {
            string extIp = null;
            try
            {
                extIp = new WebClient().DownloadString("http://icanhazip.com");
            }
            catch
            {
                extIp = "127.0.0.1";
                MessageBox.Show("I can not get your external IP address!");
            }
            finally
            {
                textServerIp.Text = extIp;
            }
        }

        private void buttonGetGccPath_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkCompilersPath("GCC", textPathGcc, 0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            checkCompilersPath("G++", textPathGpp, 1);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textPathGcc.Text = openFileDialog1.FileName;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            // Open log file
            try
            {
                logFile = new StreamWriter("log.txt", true);
                addLogMessage("App is started.");
            }

            catch
            {
                addLogMessage("Log file can not be opened!");
            }
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            serverStop();

            // Close log file
            try
            {
                addLogMessage("App is closed.");
                logFile.Close();
            }
            catch
            {
                MessageBox.Show("Log file can not be closed!");
            }

            // Remove temp files!!!
            try
            {
                File.Delete("temp.cpp");
                File.Delete("temp.exe");
            }
            catch
            {
                // Files are on disc, sorry!
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Disconnect client
            try
            {
                clientSocket.Disconnect(true);
                clientSocket.Close();
                addLogMessage("Client was disconnected.");
            }

            catch
            {
                addLogMessage("Can not close client!");
            }

            finally
            {
                button3.Enabled = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            runTimeout = int.Parse(textBox1.Text);
        }

        private void buttonGetGppPath_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            textPathGpp.Text = openFileDialog2.FileName;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            serverStart();
            acceptConnection();
        }
    }
}
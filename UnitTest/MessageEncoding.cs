﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestHacks;
using System.IO;

namespace Namespace
{
    [TestClass]
    [DeploymentItem("TestCase")]
    public class MessageEncoding : TestBase
    {
        private IEnumerable<string> Stuff
        {
            get
            {
                List<string> tempList = new List<string>();
                int numberOfFile = 1;
                while (File.Exists(numberOfFile.ToString()))
                {
                    tempList.Add(File.ReadAllText(numberOfFile.ToString()));
                    numberOfFile++;
                }

                return tempList;
            }
        }

        [TestMethod]
        [TestCategory("Message Encoding")]
        [DataSource("Namespace.MessageEncoding.Stuff")]
        public void Message_Encoding()
        {
            string input = this.TestContext.GetRuntimeDataSourceObject<string>();

            Byte[] words;
            Byte[] result;

            {
                words = Encoding.UTF8.GetBytes(input);

                Byte[] responseData = null;

                // Prepare header
                Byte[] lengthBytes = null;
                if ((words.Length <= 125) && (words.Length >= 0))
                {
                    // Message is small
                    responseData = new Byte[words.Length + 2];
                    for (int i = 0; i < words.Length; i++)
                    {
                        responseData[i + 2] = words[i];
                    }

                    responseData[0] = 129;
                    responseData[1] = (byte)(words.Length);
                }

                else if ((words.Length > 125) && (words.Length <= 65535))
                {
                    // Message is bigger
                    lengthBytes = new byte[2];
                    lengthBytes[0] = (Byte)((words.Length & 65280) >> 8);
                    lengthBytes[1] = (Byte)((words.Length & 255));

                    responseData = new Byte[words.Length + 4];
                    for (int i = 0; i < words.Length; i++)
                    {
                        responseData[i + 4] = words[i];
                    }

                    responseData[0] = 129;
                    responseData[1] = 126;
                    responseData[2] = lengthBytes[0];
                    responseData[3] = lengthBytes[1];
                }

                else if (words.Length > 65535)
                {
                    // Message is the biggest
                    lengthBytes = new byte[8];
                    lengthBytes[0] = (Byte)((words.Length & -72057594037927936) >> 56);
                    lengthBytes[1] = (Byte)((words.Length & 71776119061217280) >> 48);
                    lengthBytes[2] = (Byte)((words.Length & 280375465082880) >> 40);
                    lengthBytes[3] = (Byte)((words.Length & 1095216660480) >> 32);
                    lengthBytes[4] = (Byte)((words.Length & 4278190080) >> 24);
                    lengthBytes[5] = (Byte)((words.Length & 16711680) >> 16);
                    lengthBytes[6] = (Byte)((words.Length & 65280) >> 8);
                    lengthBytes[7] = (Byte)((words.Length & 255));

                    responseData = new Byte[words.Length + 10];
                    for (int i = 0; i < words.Length; i++)
                    {
                        responseData[i + 10] = words[i];
                    }

                    responseData[0] = 129;
                    responseData[1] = 127;
                    responseData[2] = lengthBytes[0];
                    responseData[3] = lengthBytes[1];
                    responseData[4] = lengthBytes[2];
                    responseData[5] = lengthBytes[3];
                    responseData[6] = lengthBytes[4];
                    responseData[7] = lengthBytes[5];
                    responseData[8] = lengthBytes[6];
                    responseData[9] = lengthBytes[7];
                }

                else
                {
                    result = null;
                }

                result = responseData;

                // In other way now...

                Byte[] code = responseData;
                Byte[] rawMessageData = null;
                ulong messageLength = code[1];

                if (code[0] == 129)
                {
                    if (((messageLength - 0) <= 125) && ((messageLength - 0 >= 0)))
                    {
                        rawMessageData = new Byte[messageLength];

                        for (ulong i = 0; i < messageLength; i++)
                        {
                            rawMessageData[i] = code[i + 2];
                        }
                    }

                    else if ((messageLength - 0) == 126)
                    {
                        // Message is bigger
                        messageLength = (ushort)(code[2] << 8 | code[3]);
                        rawMessageData = new Byte[messageLength];

                        for (ulong i = 0; i < messageLength; i++)
                        {
                            rawMessageData[i] = code[i + 4];
                        }
                    }

                    else if ((messageLength - 0) == 127)
                    {
                        // Message is the biggest
                        messageLength = (ulong)(code[2] << 56 | code[3] << 48 | code[4] << 40 | code[5] << 32 | code[6] << 24 | code[7] << 16 | code[8] << 8 | code[9]);
                        rawMessageData = new Byte[messageLength];

                        for (ulong i = 0; i < messageLength; i++)
                        {
                            rawMessageData[i] = code[i + 10];
                        }
                    }

                    else
                    {
                        Assert.Fail("Received data is broken!");
                    }
                }

                else
                {
                    Assert.Fail("Received data is broken!");
                }

                Assert.AreEqual(Encoding.UTF8.GetString(rawMessageData), input);
            }
   
        }
    }
}
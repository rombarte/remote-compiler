# Remote Compiler
Zestaw aplikacji typu klient/serwer do zdalnej kompilacji kodu źródłowego

## Opis aplikacji
Aplikacja **Remote Compiler** to zestaw dwóch aplikacji (klienta napisanego w języku JavaScript oraz serwera napisanego w języku C#) umożliwiającego kompilację oraz uruchamianie kodu na zdalnej maszynie (dzięki protokołowi WebSocket). Projekt powstał na zajęcia z przedmiotu **Aplikacje Sieciowe**.

## Najważniejsze funkcje
- Intuicyjny interfejs użytkownika
- Autoryzacja połaczenia hasłem
- Zapisywanie informacji o stanie w dzienniku
- Wyświetlanie informacji o błędach kompilacji
- Wyświetlanie danych ze standardowego wyjścia
- Możliwość wyboru kompilatora (C lub C++)

Kompilator musi zostać doinstalowany osobno. Jeżeli program nie wykryje kompilatora w systemie to zaproponuje jego pobranie.

## Licencja
Wszystkie pliki w tym repozytorium udostępniam na licencji MIT. Więcej informacji oraz odnośnik do treści licencji znajdziesz na stronie [Wikipedii](http://pl.wikipedia.org/wiki/Licencja_X11).
